#ifndef TIMESTAMP_VERSION_H
#define TIMESTAMP_VERSION_H

#define TIMESTAMP "timestamp"
#define STAMPIT   "stampit"

#define VERSION   "0.4.0 (2024-04-03)"

#endif
