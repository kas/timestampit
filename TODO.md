## Future plans

* Implement timedelta ([#2](https://codeberg.org/kas/timestampit/issues/2))

### Suggestions

If you feel something is missing, please open an issue on
[Codeberg](https://codeberg.org/kas/timestampit/issues).
